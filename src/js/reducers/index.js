import window from './window'
import tab from './tab'
import { combineReducers } from 'redux'

export default combineReducers({
  window,
  tab
})
